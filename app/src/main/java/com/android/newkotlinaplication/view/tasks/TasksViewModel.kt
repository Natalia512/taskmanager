package com.android.newkotlinaplication.view.tasks

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.newkotlinaplication.database.TaskRepository
import com.android.newkotlinaplication.database.model.TaskEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TasksViewModel(private val repository: TaskRepository) : ViewModel() {

    private lateinit var list: List<TaskEntity>

    private val task: MutableLiveData<List<TaskEntity>> by lazy {
        MutableLiveData<List<TaskEntity>>()
    }

    fun getTasks(): LiveData<List<TaskEntity>> {
        return task
    }

    fun loadTasks() {
        viewModelScope.launch(Dispatchers.IO) {
            list = repository.toDoDatabase.getTaskDAO().getAll()
            task.postValue(list)
        }
    }
}