package com.android.newkotlinaplication.view.edit_task

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.newkotlinaplication.database.TaskRepository
import com.android.newkotlinaplication.database.model.TaskEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EditTaskViewModel(private val taskRepository: TaskRepository) : ViewModel() {

    private val task: MutableLiveData<TaskEntity> by lazy {
        MutableLiveData<TaskEntity>()
    }

    fun saveTask(id: Int?, text: String,title:String, time: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            taskRepository.createOrUpdate(
                id,
                text,
                title,
                time
            )
        }
    }

    fun getTask(): LiveData<TaskEntity> {
        return task
    }

    fun loadTask(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            taskRepository.getTask(id)?.let {
                task.postValue(it)
            }
        }
    }
}
