package com.android.newkotlinaplication.view.tasks

import android.os.Build
import android.os.Bundle
import android.widget.Button
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.TaskApplication
import com.android.newkotlinaplication.R
import com.android.newkotlinaplication.database.model.TaskEntity
import com.android.newkotlinaplication.di.components.DaggerTasksActivityComponent
import com.android.newkotlinaplication.di.modules.TasksActivityModule
import com.android.newkotlinaplication.view.edit_task.EditTaskActivity
import javax.inject.Inject

class TasksActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var taskAdapter: TasksAdapter

    @Inject
    internal lateinit var  model: TasksViewModel

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tasks)
        val appComponent = (application as TaskApplication).appComponent

        DaggerTasksActivityComponent.builder()
            .appComponent(appComponent)
            .tasksActivityModule(TasksActivityModule())
            .build()
            .inject(this)

        taskAdapter = TasksAdapter(this)

        recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
            adapter = taskAdapter
            layoutManager = LinearLayoutManager(this@TasksActivity)
        }

        model.getTasks().observe(this, Observer<List<TaskEntity>> { tasks ->
            taskAdapter.myDataset = tasks
            taskAdapter.notifyDataSetChanged()
        })

        findViewById<Button>(R.id.add_tasks).apply {
            this.setOnClickListener {
                startActivity(EditTaskActivity.getIntent(context, null))
            }
        }
    }

    override fun onStart() {
        super.onStart()
        model.loadTasks()
    }
}
