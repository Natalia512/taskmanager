package com.android.newkotlinaplication.view.edit_task

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.CalendarView
import android.widget.EditText
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.android.TaskApplication
import com.android.newkotlinaplication.R
import com.android.newkotlinaplication.database.model.TaskEntity
import com.android.newkotlinaplication.di.components.DaggerEditTaskActivityComponent
import com.android.newkotlinaplication.di.modules.EditTaskModule
import java.util.*
import javax.inject.Inject

class EditTaskActivity : AppCompatActivity() {
    private lateinit var mytext: EditText
    private lateinit var titleText:EditText

    @Inject
    lateinit var viewModel: EditTaskViewModel
    lateinit var timePicker: TimePicker
    lateinit var calendarView: CalendarView

    var taskId: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_task)
        val appComponent = (application as TaskApplication).appComponent

        DaggerEditTaskActivityComponent.builder().appComponent(appComponent)
            .editTaskModule(EditTaskModule()).build().inject(this)

        taskId = intent.getIntExtra(ID_PARAM, 0)
        mytext = findViewById<EditText>(R.id.edit_text)
        titleText=findViewById<EditText>(R.id.titleText)
        timePicker = findViewById<TimePicker>(R.id.timePicker)
        calendarView = findViewById(R.id.calendarView)

        val calendar = Calendar.getInstance()

        calendarView.setOnDateChangeListener { _, year, month, dayOfMonth ->
            Log.i("DATA2", "${Date(calendarView.date)}")
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        }

        timePicker.setOnTimeChangedListener { _, hourOfDay, minute ->
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
            calendar.set(Calendar.MINUTE, minute)
        }

        viewModel.getTask().observe(this, Observer<TaskEntity> {
            mytext.setText(it.text)
            titleText.setText(it.title)
            val savedCalendar = Calendar.getInstance()
            savedCalendar.timeInMillis = it.time

            Log.i("DATA3", "${savedCalendar.time}")

            calendarView.date = savedCalendar.time.time
            timePicker.setIs24HourView(true)
            timePicker.currentMinute = savedCalendar.get(Calendar.MINUTE)
            timePicker.currentHour = savedCalendar.get(Calendar.HOUR_OF_DAY)

            Log.i("DATA1", "${savedCalendar.time.time}")
        })

        findViewById<Button>(R.id.save_button).apply {
            setOnClickListener {
                viewModel.saveTask(taskId, mytext.text.toString(),titleText.text.toString(), calendar.time.time)
                Log.i("DATA", "${calendar.time.time}")
            }
        }
    }

    override fun onStart() {
        super.onStart()
        taskId?.let { viewModel.loadTask(it) }
    }

    companion object {
        const val ID_PARAM = "id"

        fun getIntent(context: Context, taskId: Int?): Intent {
            return Intent(context, EditTaskActivity::class.java).apply {
                this.putExtra(
                    ID_PARAM,
                    taskId
                )
            }
        }
    }
}




