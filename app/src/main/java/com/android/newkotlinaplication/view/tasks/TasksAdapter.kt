package com.android.newkotlinaplication.view.tasks

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.newkotlinaplication.R
import com.android.newkotlinaplication.database.model.TaskEntity
import com.android.newkotlinaplication.view.edit_task.EditTaskActivity

class TasksAdapter(private val context: Context, var myDataset: List<TaskEntity> = ArrayList()) :
    RecyclerView.Adapter<TasksAdapter.MyViewHolder>() {

    class MyViewHolder(val textView: TextView) : RecyclerView.ViewHolder(textView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val textView = LayoutInflater.from(context)
            .inflate(R.layout.item_task, parent, false) as TextView
        return MyViewHolder(textView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.textView.text = myDataset[position].title
        holder.itemView.setOnClickListener {
            context.startActivity(EditTaskActivity.getIntent(context, myDataset[position].id))
        }
    }

    override fun getItemCount(): Int {
        return myDataset.size
    }
}