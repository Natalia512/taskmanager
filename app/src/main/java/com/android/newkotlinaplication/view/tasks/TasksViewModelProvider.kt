package com.android.newkotlinaplication.view.tasks

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.newkotlinaplication.database.TaskRepository
import com.android.newkotlinaplication.view.edit_task.EditTaskViewModel

class TasksViewModelProvider(
    private val repository: TaskRepository,
    val application: Application
) :
    ViewModelProvider.AndroidViewModelFactory(application) {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == EditTaskViewModel::class.java) {
            return EditTaskViewModel(
                repository
            ) as T
        } else if (modelClass == TasksViewModel::class.java) {
            return TasksViewModel(
                repository
            ) as T
        }

        throw Exception("Type is not found")
    }
}