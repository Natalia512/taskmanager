package com.android.newkotlinaplication.di.modules

import android.app.Application
import com.android.newkotlinaplication.database.TaskRepository
import com.android.newkotlinaplication.view.tasks.TasksViewModelProvider
import dagger.Module
import dagger.Provides

@Module
class ProvideViewModelModule {

    @Provides
    fun provideViewModelFactory(
        repository: TaskRepository,
        application: Application
    ): TasksViewModelProvider {
        return TasksViewModelProvider(
            repository,
            application
        )
    }
}