package com.android.newkotlinaplication.di.modules

import com.android.newkotlinaplication.view.tasks.TasksViewModel
import com.android.newkotlinaplication.view.tasks.TasksViewModelProvider
import dagger.Module
import dagger.Provides

@Module(includes = [ProvideViewModelModule::class])
class TasksActivityModule {

    @Provides
    fun provideTaskViewModel(viewModelProvider: TasksViewModelProvider): TasksViewModel {
        return viewModelProvider.create(TasksViewModel::class.java)
    }
}