package com.android.newkotlinaplication.di.components

import android.app.Application
import com.android.newkotlinaplication.di.components.scope.ApplicationScope
import com.android.newkotlinaplication.di.modules.ApplicationModule
import com.android.newkotlinaplication.database.ToDoDatabase
import dagger.Component

@Component(modules = [ApplicationModule::class])
@ApplicationScope
interface AppComponent {

   fun getToDoDatabase() : ToDoDatabase

   fun getApplication() : Application

}