package com.android.newkotlinaplication.di.components

import com.android.newkotlinaplication.di.components.scope.ActivityScope
import com.android.newkotlinaplication.di.modules.TasksActivityModule
import com.android.newkotlinaplication.view.tasks.TasksActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [TasksActivityModule::class])
interface TasksActivityComponent {

    fun inject(primaryActivity: TasksActivity)
}