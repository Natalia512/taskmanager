package com.android.newkotlinaplication.di.modules

import android.app.Application
import androidx.room.Room
import com.android.newkotlinaplication.di.components.scope.ApplicationScope
import com.android.newkotlinaplication.database.ToDoDatabase
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @ApplicationScope
    fun provideDB(application: Application): ToDoDatabase {
        return Room.databaseBuilder(application, ToDoDatabase::class.java, DB_NAME).build()
    }

    @Provides
    fun provideContext(): Application {
        return application
    }

    companion object {
        private const val DB_NAME = "tasksDatabase"
    }
}