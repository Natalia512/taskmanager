package com.android.newkotlinaplication.di.components

import com.android.newkotlinaplication.di.components.scope.ActivityScope
import com.android.newkotlinaplication.di.modules.EditTaskModule
import com.android.newkotlinaplication.view.edit_task.EditTaskActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [EditTaskModule::class])
interface EditTaskActivityComponent {
    fun inject(editTaskActivity: EditTaskActivity)
}