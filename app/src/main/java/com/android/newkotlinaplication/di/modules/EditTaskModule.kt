package com.android.newkotlinaplication.di.modules

import com.android.newkotlinaplication.view.edit_task.EditTaskViewModel
import com.android.newkotlinaplication.view.tasks.TasksViewModelProvider
import dagger.Module
import dagger.Provides

@Module(includes = [ProvideViewModelModule::class])
class EditTaskModule {

    @Provides
    fun provideActivityViewModel(viewModelProvider: TasksViewModelProvider): EditTaskViewModel {
        return viewModelProvider.create(EditTaskViewModel::class.java)
    }

}