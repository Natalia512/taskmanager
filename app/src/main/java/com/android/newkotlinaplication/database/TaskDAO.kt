package com.android.newkotlinaplication.database;

import androidx.room.*
import com.android.newkotlinaplication.database.model.TaskEntity

@Dao
interface TaskDAO {

    @Query("SELECT * FROM tasks")
    suspend fun getAll(): List<TaskEntity>

    @Query("SELECT * FROM tasks WHERE id = :id")
    suspend fun getTask(id: Int): TaskEntity?

    @Insert
    suspend fun insert(tasks: TaskEntity)

    @Delete
    suspend fun delete(task: TaskEntity)

    @Update
    suspend fun update(task: TaskEntity)
}