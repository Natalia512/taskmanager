package com.android.newkotlinaplication.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.newkotlinaplication.database.model.TaskEntity

@Database(entities = [TaskEntity::class], version = 1, exportSchema = false)
abstract class ToDoDatabase : RoomDatabase() {
    abstract fun getTaskDAO(): TaskDAO
}