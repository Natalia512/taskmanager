package com.android.newkotlinaplication.database

import com.android.newkotlinaplication.database.model.TaskEntity
import javax.inject.Inject

class TaskRepository @Inject constructor(val toDoDatabase: ToDoDatabase) {

    suspend fun createOrUpdate(id: Int?, text: String, title:String, time: Long) {
        val taskDAO = toDoDatabase.getTaskDAO()

        val task = TaskEntity(id ?: 0, text, title, time)
        if (id == null || taskDAO.getTask(id) == null) {
            taskDAO.insert(task)
        } else {
            taskDAO.update(task)
        }
    }

    suspend fun getTask(id: Int): TaskEntity? {
        return toDoDatabase.getTaskDAO().getTask(id)
    }
}