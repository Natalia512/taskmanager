package com.android.newkotlinaplication.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tasks")
data class TaskEntity (
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") var id: Int = 0,
    @ColumnInfo(name = "text") var text: String = "",
    @ColumnInfo(name = "title") var title: String = "",
    @ColumnInfo(name = "time") var time: Long
)