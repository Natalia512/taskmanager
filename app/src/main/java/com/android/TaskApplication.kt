package com.android

import android.app.Application
import com.android.newkotlinaplication.di.components.AppComponent
import com.android.newkotlinaplication.di.components.DaggerAppComponent
import com.android.newkotlinaplication.di.modules.ApplicationModule

class TaskApplication : Application() {

    lateinit var appComponent: AppComponent
    override fun onCreate() {
        super.onCreate()
        appComponent = buildComponent()
    }

    private fun buildComponent(): AppComponent {
        return DaggerAppComponent.builder().applicationModule(ApplicationModule(this)).build()
    }
}